﻿using System;
namespace CCIChallenge
{
    /// <summary>
    /// Class which contains the attributes of Products type.
    /// </summary>
    public class Products
    {
        public string Code { get; set; }
        public string ProductNumber { get; set; }
        public string Name { get; set; }
        public string AdvicedPrice { get; set; }
        public string Category { get; set; }
        public string CountryCode { get; set; }
    }
}
