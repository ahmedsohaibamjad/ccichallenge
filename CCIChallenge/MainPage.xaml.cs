﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CCIChallenge
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        /// <summary>
        /// Main Page to navigate into the list view
        /// </summary>
        public MainPage()
        {
            InitializeComponent();

            btnEnter.Clicked += (sender, e) =>
            {
                var productListView = new ProductListView();
                Navigation.PushAsync(productListView);
            };
        }
    }
}
