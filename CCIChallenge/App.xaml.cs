﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCIChallenge
{
    /// <summary>
    /// Delegate to fetch the products.
    /// </summary>
    /// <param name="productsList"></param>
    public delegate void GetProductListDelegate(List<Products> productsList);
    /// <summary>
    /// delegate to show the errors.
    /// </summary>
    /// <param name="error"></param>
    public delegate void ErrorDelegateMethod(string error);

    public partial class App : Application
    {
        /// <summary>
        /// Root Page for navigation stack
        /// </summary>
        public static ContentPage rootPage;

        public App()
        {
            InitializeComponent();

            rootPage = new MainPage();
            NavigationPage navPage = new NavigationPage(rootPage);
            MainPage = navPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
