﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

namespace CCIChallenge
{
    /// <summary>
    /// Http service to get the products from API, Parse the JSON and store it
    /// into the list of Products type
    /// </summary>
    public class GetProdcutsListService
    {
        List<Products> Products = new List<Products>();

        public GetProductListDelegate productsDele;
        public ErrorDelegateMethod errDele;

        /// <summary>
        /// Method which takes the uri and executes the service on background
        /// to fetch the data
        /// </summary>
        public void FetchProducts()
        {
            string productsURL = "https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/4dcea520-7b15-4334-81de-ceb4e3813016/cci-mock-api/1.0.4/m/json/sf-json?client_id=123456&client_secret=45454787";

            ExecuteService(productsURL);
        }

        /// <summary>
        /// Execute service to get the data asynchronosly from the endpoint
        /// </summary>
        /// <param name="productsURL"></param>
        public async void ExecuteService(string productsURL)
        {
            HttpClient client = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(30)
            };

#if __ANDROID__
            client.DefaultRequestHeaders.Accept
                .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
#endif

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.GetAsync(productsURL);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                errDele("Error while fetching products");
                return;
            }

            if(response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    string result = await content.ReadAsStringAsync();
                    var jsonObj = JsonConvert.DeserializeObject<List<Products>>(result);
                    productsDele(jsonObj);
                }
            }
            else
            {
                Console.WriteLine("Response Code:" + response.StatusCode);
                errDele("Response Code:" + response.StatusCode);
            }
        }

    }
}
