﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CCIChallenge
{
    public partial class ProductDetailsView : ContentPage
    {
        /// <summary>
        /// Class to show the details of the product into the detials view.
        /// </summary>
        /// <param name="product"></param>
        public ProductDetailsView(Products product)
        {
            InitializeComponent();

            PopulateUI(product);
        }

        /// <summary>
        /// Method to populate the labels on the details view.
        /// </summary>
        /// <param name="product"></param>
        void PopulateUI(Products product)
        {
            lblName.Text = product.Name;
            lblPrice.Text = product.AdvicedPrice;
            lblCode.Text = product.Code;
            lblproductNum.Text = product.ProductNumber;
            lblCategory.Text = product.Category;
            lblCC.Text = product.CountryCode;
        }
    }
}
