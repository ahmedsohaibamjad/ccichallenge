﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CCIChallenge
{
    public partial class ProductListView : ContentPage
    {
        /// <summary>
        /// Class to get the list of products from the delegate and display into
        /// the list view here.
        /// </summary>
        public GetProdcutsListService productsListInstance;

        public ProductListView()
        {
            InitializeComponent();

            IsBusy = true;
            DelegateCall();
        }

        /// <summary>
        /// Method to recieve data from the successful delegate call.
        /// </summary>
        /// <param name="productsList"></param>
        void DoneFetchingProducts(List<Products> productsList)
        {
            IsBusy = false;
            var products = productsList;
            populateListView(products);
        }

        /// <summary>
        /// Method to show an error if delegate encounters any problem.
        /// </summary>
        /// <param name="errMsg"></param>
        void ErrorFetchingProducts(string errMsg)
        {
            Console.WriteLine(errMsg);
        }

        /// <summary>
        /// Delegate call to fetch the data from API.
        /// </summary>
        public void DelegateCall()
        {
            productsListInstance = new GetProdcutsListService
            {
                productsDele = new GetProductListDelegate(DoneFetchingProducts),
                errDele = new ErrorDelegateMethod(ErrorFetchingProducts)
            };
            productsListInstance.FetchProducts();
        }

        /// <summary>
        /// Method to populate ListView.
        /// </summary>
        /// <param name="productsList"></param>
        public void populateListView(List<Products> productsList)
        {
            IsBusy = false;
            listProducts.ItemsSource = productsList;
        }

        /// <summary>
        /// Method to navigate to details view on item select.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void listProducts_ItemSelected(System.Object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var product = (Products)e.SelectedItem;
            var productDetails = new ProductDetailsView(product);
            Navigation.PushAsync(productDetails);
        }
    }
}
